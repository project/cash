
Cash 1.0.0-dev, 2024-09-13
--------------------------
- Issue #3474079 by anwar_max, sourav_paul: Too few arguments to function
  in /var/www/html/web/modules/contrib/cash/src/Form/CashSettingsForm.php

Cash 1.0.0-dev, 2024-03-18
--------------------------
- Issue #3429114 by Project Update Bot: Automated Drupal 11 compatibility fixes
  for cash.

Cash 1.0.0-dev, 2022-02-10
--------------------------
- Added root parameter to Cash service.

Cash 1.0.0-dev, 2022-02-10
--------------------------
Initial commit.
