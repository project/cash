<?php

namespace Drupal\cash\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the Cash admin settings form.
 */
class CashSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Asset\LibraryDiscoveryInterface definition.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->libraryDiscovery = $container->get('library.discovery');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cash_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cash.settings'];
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cash.settings');

    $form['sitewide'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable Cash sitewide'),
      '#default_value' => $config->get('sitewide'),
      '#description'   => $this->t('Useful if you work extensively with vanilla JavaScript at theme level.'),
    ];

    $form['unminified'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Use unminified version'),
      '#default_value' => $config->get('unminified'),
      '#description'   => $this->t('Useful to debug errors. Be sure to uncheck at production.'),
    ];

    $form['cdn'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('CDN version'),
      '#default_value' => $config->get('cdn'),
      '#max'           => 16,
      '#description'   => $this->t('Example: <b>8.1.5</b>. If provided or not empty, the CDN file will be loaded instead. Check out available releases <a href=":url">here</a>.', [
        ':url' => 'https://github.com/fabiospampinato/cash/releases',
      ]),
    ];

    $polyfill = $config->get('polyfills');
    $form['polyfills'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Polyfills'),
      '#empty_option'  => '- None -',
      '#options' => [
        'polyfill' => $this->t('Basic polyfills (ie9-ie11)'),
        'classlist' => $this->t('classList polyfill (ie9-ie11)'),
        'promise' => $this->t('Promise polyfill (ie11)'),
        'raf' => $this->t('requestAnimationFrame polyfill (ie9)'),
      ],
      '#default_value' => !empty($polyfill) ? array_values((array) $polyfill) : [],
      '#description'   => $this->t("Enable to load them if you support IEs and other oldies, or do not have polyfills at your theme globally. Only loaded (total 3.6KB) if cash is loaded. Basic polyfills: <code>Object.assign, closest, forEach, matches, startsWith, CustomEvent</code>. If using Blazy, they will be replaced here to avoid useless double loads -- the files are similar, only different namespaces. For other polyfills, due to questionabe licenses, include them into your theme as needed such as <a href=':io'>IntersectionObserver</a>, etc. <a href=':url'>Read more</a>.", [
        ':io' => 'https://github.com/w3c/IntersectionObserver',
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $cdn = trim($form_state->getValue('cdn') ?? '');

    // Respects some serious guys.
    if ($cdn) {
      $cdn = UrlHelper::filterBadProtocol($cdn);
    }

    $this->configFactory->getEditable('cash.settings')
      ->set('sitewide', $form_state->getValue('sitewide'))
      ->set('unminified', $form_state->getValue('unminified'))
      ->set('cdn', $cdn)
      ->set('polyfills', $form_state->getValue('polyfills'))
      ->save();

    // Invalidate the library discovery cache to update new assets.
    // @todo D11 $this->libraryDiscovery->clearCachedDefinitions();
    $this->configFactory->clearStaticCache();

    $this->messenger()->addMessage($this->t('Be sure to <a href=":clear_cache">clear the cache</a> if trouble to see the updated settings.', [
      ':clear_cache' => Url::fromRoute('system.performance_settings')->toString(),
    ]));

    parent::submitForm($form, $form_state);
  }

}
