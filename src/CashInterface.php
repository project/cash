<?php

namespace Drupal\cash;

/**
 * Provides common blazy utility methods.
 */
interface CashInterface {

  /**
   * Returns the app root.
   *
   * @return string
   *   The app root.
   */
  public function root();

  /**
   * Returns the config factory service.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The config factory.
   */
  public function configFactory();

  /**
   * Return the config object.
   */
  public function getConfig($config = 'cash.settings');

  /**
   * Return the config object identified by $key.
   */
  public function getSetting($key, $name = 'cash.settings');

  /**
   * Returns path matcher service.
   */
  public function theme();

  /**
   * Return a shortcut for the default theme.
   */
  public function themeDefault(): string;

  /**
   * Returns the cash path.
   */
  public function path($name = 'cash-dom'): ?string;

  /**
   * Provides a wrapper to replace deprecated libraries_get_path() at ease.
   */
  public function getLibraryPath($name, $base_path = FALSE);

  /**
   * Checks if we are using the default theme.
   */
  public function isApplicable(): bool;

  /**
   * Implements hook_library_info_alter().
   */
  public function libraryInfoAlter(&$libraries, $extension): void;

  /**
   * Implements hook_library_info_build().
   */
  public function libraryInfoBuild(): array;

  /**
   * Returns the commonly used path, or just the base path.
   *
   * @todo remove drupal_get_path check when min D9.3.
   */
  public function getPath($type, $name, $absolute = FALSE);

}
