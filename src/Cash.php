<?php

namespace Drupal\cash;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Cash utilities.
 */
class Cash implements CashInterface {

  /**
   * The app root.
   *
   * @var string
   */
  protected $root;

  /**
   * Config Factory Service Object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The library path.
   *
   * @var string
   */
  protected $path;

  /**
   * If applicable.
   *
   * @var bool
   */
  protected $isApplicable;

  /**
   * The library info.
   *
   * @var array
   */
  private $libraryInfoBuild;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $theme;

  /**
   * Constructs a Cash object.
   */
  public function __construct($root, ConfigFactoryInterface $config_factory, ThemeManagerInterface $theme) {
    $this->root = $root;
    $this->configFactory = $config_factory;
    $this->theme = $theme;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      self::rootContainer($container),
      $container->get('config.factory'),
      $container->get('theme.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function root() {
    return $this->root;
  }

  /**
   * {@inheritdoc}
   */
  public function configFactory() {
    return $this->configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig($config = 'cash.settings') {
    return $this->configFactory->get($config);
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($key, $name = 'cash.settings') {
    return $this->getConfig($name)->get($key);
  }

  /**
   * {@inheritdoc}
   */
  public function theme() {
    return $this->theme;
  }

  /**
   * {@inheritdoc}
   */
  public function themeDefault(): string {
    return $this->configFactory->get('system.theme')->get('default') ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function path($name = 'cash-dom'): ?string {
    if (!isset($this->path[$name])) {
      $this->path[$name] = $this->getLibraryPath($name);
    }
    return $this->path[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraryPath($name, $base_path = FALSE) {
    $finder = self::service('library.libraries_directory_file_finder');
    if ($finder) {
      return $finder->find($name);
    }

    $function = 'libraries_get_path';
    return is_callable($function) ? $function($name, $base_path) : '';
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(): bool {
    if (!isset($this->isApplicable)) {
      $theme = $this->theme->getActiveTheme()->getName();
      $this->isApplicable = !InstallerKernel::installationAttempted()
        && $this->themeDefault() == $theme;
    }
    return $this->isApplicable;
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoAlter(&$libraries, $extension): void {
    $file = FALSE;
    $polyfills = $this->getSetting('polyfills');

    if ($extension === 'cash') {
      $min = $this->getSetting('unminified') ? '' : '.min';

      if ($cdn = trim($this->getSetting('cdn') ?? '')) {
        $file = 'https://cdnjs.cloudflare.com/ajax/libs/cash/' . $cdn . '/cash' . $min . '.js';
        $params = ['type' => 'external', 'weight' => -19];
      }
      else {
        $params = ['minified' => !empty($min), 'weight' => -19];
        if ($path = $this->path()) {
          $file = '/' . $path . '/dist/cash' . $min . '.js';
        }
      }

      foreach (self::polyfills() as $id) {
        if (!empty($polyfills[$id])) {
          $libraries['cash']['dependencies'][] = 'cash/' . $id;
        }
      }

      if ($file) {
        $libraries['cash']['js'][$file] = $params;
        unset($libraries['cash']['js']['/libraries/cash/dist/cash.min.js']);
      }
    }

    // Replace Blazy polyfills to avoid useless double downloads.
    if ($extension === 'blazy') {
      $cash = $this->getPath('module', 'cash', TRUE);
      foreach (self::polyfills() as $id) {
        $weight = $id == 'polyfill' ? -21 : -20;
        $common = ['minified' => TRUE, 'weight' => $weight];
        if (isset($libraries[$id])) {
          $libraries[$id]['js'][$cash . '/js/polyfill/' . $id . '.min.js'] = $common;
          unset($libraries[$id]['js']['js/polyfill/blazy.' . $id . '.min.js']);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function libraryInfoBuild(): array {
    if (!isset($this->libraryInfoBuild)) {
      $libraries = [];
      // Optional polyfills for IEs, and oldies.
      foreach (self::polyfills() as $id) {
        // Matches common core polyfills' weight.
        $weight = $id == 'polyfill' ? -21 : -20;
        $common = ['minified' => TRUE, 'weight' => $weight];
        $libraries[$id] = [
          'js' => [
            'js/polyfill/' . $id . '.min.js' => $common,
          ],
        ];
      }

      $this->libraryInfoBuild = $libraries;
    }
    return $this->libraryInfoBuild;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath($type, $name, $absolute = FALSE): string {
    if ($resolver = self::service('extension.path.resolver')) {
      $path = $resolver->getPath($type, $name);
    }
    else {
      $function = 'drupal_get_path';
      $path = is_callable($function) ? $function($type, $name) : '';
    }
    return $absolute ? \base_path() . $path : $path;
  }

  /**
   * Returns available nojs components related to core Blazy functionality.
   */
  public static function polyfills(): array {
    return [
      'polyfill',
      'classlist',
      'fullscreen',
      'promise',
      'raf',
    ];
  }

  /**
   * Returns the cross-compat D8 ~ D10 app root.
   */
  public static function rootContainer($container) {
    return version_compare(\Drupal::VERSION, '9.0', '<')
      ? $container->get('app.root') : $container->getParameter('app.root');
  }

  /**
   * Returns a wrapper to pass tests, or DI where adding params is troublesome.
   */
  public static function service($service) {
    return \Drupal::hasService($service) ? \Drupal::service($service) : NULL;
  }

}
