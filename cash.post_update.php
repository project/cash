<?php

/**
 * @file
 * Post update hooks for Cash.
 */

/**
 * Clear cache to add root to cash service.
 */
function cash_post_update_add_root_parameter() {
  // Empty hook to clear caches.
}
