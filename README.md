
# <a name="top"> </a>CONTENTS OF THIS FILE

 * [Introduction](#introduction)
 * [Requirements](#requirements)
 * [Installation](#installation)
 * [Installing libraries via Composer](#composer)
 * [Configuration](#configuration)
 * [Usage](#usage)
 * [Migration guide](https://github.com/fabiospampinato/cash/blob/master/docs/migration_guide.md)
 * [Maintainers](#maintainers)

## <a name="introduction"></a>INTRODUCTION

An absurdly small jQuery alternative for modern browsers (IE9+).

Cash is a small library for modern browsers (Chrome, Firefox, Safari and
Internet Explorer 9+) that provides jQuery style syntax for manipulating the
DOM. Utilizing modern browser features to minimize the codebase, developers can
use the familiar chainable methods at a fraction of the file size.
100% feature parity with jQuery isn't a goal, but cash comes helpfully close,
covering most day to day use cases.


### Size Comparison
| Size               | Cash        | Zepto 1.2.0 | jQuery Slim 3.4.1 |
| ------------------ | ----------- | ----------- | ----------------- |
| Unminified         | **36.5 KB** | 58.7 KB     | 227 KB            |
| Minified           | **16 KB**   | 26 KB       | 71 KB             |
| Minified & Gzipped | **6 KB**    | 9.8 KB      | 24.4 KB           |


If you need to support old browsers (IE8 below), Cash is not for you.

## <a name="requirements"></a>REQUIREMENTS
[Cash](https://github.com/kenwheeler/cash) library:  
Download Cash and extract it as is, rename `cash-master` to `cash`, so the
assets are at any:  

  + `/libraries/cash/dist/cash.min.js` or
  + `/libraries/cash-dom/dist/cash.min.js`  

  Both paths are supported. The first is if downloaded directly from Github.
  The latter if using Composer via asset-packages and or via `npm-asset`.
  Or any path supported by core library finder or libraries.module.


## <a name="installation"></a>INSTALLATION
Install the module as usual, more info can be found on:

[Installing Drupal 8 Modules](https://drupal.org/node/1897420)


## <a name="composer"></a>INSTALLING THIRD-PARTY LIBRARIES VIA COMPOSER

Until Drupal has an official [core management of the 3rd-party front-end libraries](https://www.drupal.org/project/drupal/issues/2873160), there are various ways to
install third party bower (deprecated)/ npm asset libraries.

Check out any below suitable to your workflow:  

  + [#3021902](https://www.drupal.org/project/blazy/issues/3021902)
  + [#2907371](https://www.drupal.org/project/slick/issues/2907371)
  + [#2907371](https://www.drupal.org/project/slick/issues/2907371#comment-12882235)  
  + Via [asset-packagist.org](https://asset-packagist.org/), see below.

It is up to you to decide which works best. Composer is not designed to
manage JS, CSS or HTML framework assets. It is for PHP. Then come Composer
plugins, and other workarounds to make Composer workflow easier. As many
alternatives, it is not covered here. Please find more info on the
above-mentioned issues.


## VIA ASSET-PACKAGIST.ORG
If using [asset-packagist.org](https://asset-packagist.org/), regardless cons,
be sure to set up your composer.json correctly, some distros use it, see
[Slick](https://drupal.org/project/slick) project for the supporting distros to
copy/ paste from their composer.json files. Be warned! Invalid json may break.
Normally unwanted trailing commas.

1. Add/ merge these lines, add commas as required:  
````
    "repositories": [
        {
            "type": "composer",
            "url": "https://asset-packagist.org"
        }
    ]
````

2. Add/ merge these lines, add commas as required:  
````
    "extra": {
        "installer-types": [
            "bower-asset",
            "npm-asset"
        ],
        "installer-paths": {
            "web/libraries/{$name}": [
                "type:drupal-library",
                "type:bower-asset",
                "type:npm-asset"
            ],
            // ....
        }
    }
````

3. Require [composer-installers-extender](https://github.com/oomphinc/composer-installers-extender):  
  `composer require oomphinc/composer-installers-extender`

4. Then require any libraries as usual only prefixed with `npm-asset`, or
   `bower-asset` (deprecated). The versions must be re-checked, just samples:
   + Install [Cash](https://www.drupal.org/project/cash):  
     `composer require npm-asset/cash`
   + etc.

5. To update:  
   `composer update --with-dependencies`


## <a name="configuration"> </a>CONFIGURATION
Visit the following to configure and make use of Cash:

* `/admin/config/media/cash`

##  <a name="usage"> </a>USAGE
The module doesn't do anything on its own by default.

Include it as needed using regular library attachments, e.g.:

`$page['#attached']['library'][] = 'cash/cash';`  

Or use the provided UI for convenient at:  

`/admin/config/media/cash`

Or use it as your own library dependency, recommended:
```
my_library:
  js:
    js/my_theme.min.js: {}
  dependencies:
    - cash/cash
```

Use it like `jQuery`, only now replace `jQuery` with `Cash`. Keep the `$` dollar
sign as usual. No need to change it.

If you're migrating from jQuery be sure to read our
[migration guide](https://github.com/fabiospampinato/cash/blob/master/docs/migration_guide.md).


### WHO IS THIS MODULE FOR?
For anyone who work with vanilla JavaScript rather than jQuery.


## <a name="maintainers"> </a>MAINTAINERS/CREDITS
* [Gaus Surahman](https://www.drupal.org/user/159062)
* [Contributors](https://www.drupal.org/node/2837477/committers)
* CHANGELOG.txt for helpful souls with their patches, suggestions and reports.


### READ MORE
See the project page on drupal.org, or github.com, for more up to date info:

* [Cash module](https://www.drupal.org/project/cash)
* [Initial Cash library](https://github.com/kenwheeler/cash)
* [Current Cash library](https://github.com/fabiospampinato/cash)
